#!/bin/bash -eu

echo "--- Building Jar"
./mvnw package

echo "--- Building docker image"
docker build -t "dt0814/merchant-configuration" .

echo "--- Push docker image"
docker push dt0814/merchant-configuration:latest
