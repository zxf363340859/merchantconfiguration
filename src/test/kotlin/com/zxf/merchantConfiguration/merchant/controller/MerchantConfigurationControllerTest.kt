package com.zxf.merchantConfiguration.merchant.controller

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.zxf.merchantConfiguration.merchant.enum.MerchantStatus
import com.zxf.merchantConfiguration.merchant.common.generateMerchantConfiguration
import com.zxf.merchantConfiguration.merchant.repository.MerchantConfigurationRepository
import com.zxf.merchantConfiguration.merchant.response.MerchantConfigurationResponse
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@SpringBootTest
@AutoConfigureMockMvc
class MerchantConfigurationControllerTest {
    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var merchantConfigurationRepository: MerchantConfigurationRepository

    val objectMapper = jacksonObjectMapper()

    @BeforeEach
    internal fun setUp() {
        merchantConfigurationRepository.deleteAll()
    }

    @Test
    internal fun `should be return 201 and location`() {
        mockMvc.perform(
                post("/merchant-configuration")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateMerchantConfiguration()))
        )
                .andExpect(status().isCreated)
                .andExpect(header().exists("location"))
        val all = merchantConfigurationRepository.findAll()
        assert(all.isNotEmpty())
    }


    @Test
    internal fun `should be return 400 when not give necessary field`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        generateMerchantConfiguration.name = ""
        mockMvc.perform(
                post("/merchant-configuration")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateMerchantConfiguration))
        )
                .andExpect(status().isBadRequest)

    }

    @Test
    internal fun `should be return 400 when give invalid status value`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        generateMerchantConfiguration.status = MerchantStatus.Active
        val json = objectMapper.writeValueAsString(generateMerchantConfiguration).replace("Active", "active1")
        mockMvc.perform(
                post("/merchant-configuration")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        )
                .andExpect(status().isBadRequest)
    }

    @Test
    internal fun `should be return 400 when give invalid merchant id`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        generateMerchantConfiguration.merchantId = 0
        mockMvc.perform(
                post("/merchant-configuration")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateMerchantConfiguration))
        )
                .andExpect(status().isBadRequest)
    }

    @Test
    internal fun `should be return all merchant when give valid invoice date and status`() {
        Assertions.assertEquals(merchantConfigurationRepository.findAll().size, 0)
        val merchantConfiguration1 = generateMerchantConfiguration()
        merchantConfiguration1.merchantId = 1
        val merchantConfiguration2 = generateMerchantConfiguration()
        merchantConfiguration2.merchantId = 2
        merchantConfigurationRepository.saveAll(
                listOf(
                        merchantConfiguration1.toEntity(),
                        merchantConfiguration2.toEntity()
                )
        )
        merchantConfigurationRepository.flush()
        val response = mockMvc.perform(
                get("/merchant-configuration"))
                .andExpect(status().isOk).andReturn().response
        val merchantConfigurations: List<MerchantConfigurationResponse> = objectMapper.readValue(response.contentAsString)
        Assertions.assertEquals(merchantConfigurations.size, 2)
        Assertions.assertEquals(merchantConfigurations[0].name, merchantConfiguration1.name)
        Assertions.assertEquals(merchantConfigurations[1].name, merchantConfiguration2.name)
    }

    @Test
    internal fun `should be return empty when give invoice date and database not have match invoice date`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        generateMerchantConfiguration.invoiceDate =
                Date(generateMerchantConfiguration.invoiceDate.time - 1000 * 60 * 60 * 24)
        merchantConfigurationRepository.saveAndFlush(generateMerchantConfiguration.toEntity())
        mockMvc.perform(
                get("/merchant-configuration")
                        .param("invoiceDate", Date().time.toString()))
                .andExpect(status().isOk)
                .andExpect(content().string("[]"))
        assert(merchantConfigurationRepository.findAll().isNotEmpty())
    }

    @Test
    internal fun `should be return list when give invoice date and database have match invoice date`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        merchantConfigurationRepository.saveAndFlush(generateMerchantConfiguration.toEntity())
        mockMvc.perform(
                get("/merchant-configuration")
                        .param("invoiceDate", generateMerchantConfiguration.invoiceDate.time.toString()))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$[0].name").value(generateMerchantConfiguration.name))
                .andExpect(jsonPath("$[0].status").value(generateMerchantConfiguration.status.name))
                .andExpect(jsonPath("$[0].description").value(generateMerchantConfiguration.description))
        assert(merchantConfigurationRepository.findAll().isNotEmpty())
    }

    @Test
    internal fun `should be return empty when give a has invoice date and status is disable`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        generateMerchantConfiguration.status = MerchantStatus.Disable
        merchantConfigurationRepository.saveAndFlush(generateMerchantConfiguration.toEntity())
        mockMvc.perform(
                get("/merchant-configuration")
                        .param("invoiceDate", generateMerchantConfiguration.invoiceDate.time.toString()))
                .andExpect(status().isOk)
                .andExpect(content().string("[]"))
        assert(merchantConfigurationRepository.findAll().isNotEmpty())
    }

    @Test
    internal fun `should be return list when give valid invoice date and status`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        generateMerchantConfiguration.status = MerchantStatus.Disable
        merchantConfigurationRepository.saveAndFlush(generateMerchantConfiguration.toEntity())

        val response = mockMvc.perform(
                get("/merchant-configuration")
                        .param("invoiceDate", generateMerchantConfiguration.invoiceDate.time.toString())
                        .param("status", "Disable")
        )
                .andExpect(status().isOk).andReturn().response

        val merchantConfigurations: List<MerchantConfigurationResponse> = objectMapper.readValue(response.contentAsString)
        Assertions.assertEquals(merchantConfigurations.size, 1)
        Assertions.assertEquals(merchantConfigurations[0].name, generateMerchantConfiguration.name)
        Assertions.assertEquals(merchantConfigurations[0].status, generateMerchantConfiguration.status.name)
        assert(merchantConfigurationRepository.findAll().isNotEmpty())
    }

    @Test
    internal fun `should be return list when give valid status`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        generateMerchantConfiguration.status = MerchantStatus.Disable
        merchantConfigurationRepository.saveAndFlush(generateMerchantConfiguration.toEntity())
        mockMvc.perform(
                get("/merchant-configuration")
                        .param("status", "Disable")
        )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$[0].name").value(generateMerchantConfiguration.name))
                .andExpect(jsonPath("$[0].status").value(generateMerchantConfiguration.status.name))
        assert(merchantConfigurationRepository.findAll().isNotEmpty())
    }

    @Test
    internal fun `should be return 204 when send put request`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        merchantConfigurationRepository.saveAndFlush(generateMerchantConfiguration.toEntity())
        generateMerchantConfiguration.name = "update Name"
        mockMvc.perform(
                put("/merchant-configuration")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateMerchantConfiguration))
        )
                .andExpect(status().isNoContent)
        val merchantConfiguration = merchantConfigurationRepository.findById(generateMerchantConfiguration.merchantId)
        Assertions.assertEquals(merchantConfiguration.get().name, "update Name")
    }

    @Test
    internal fun `should be return 404 when send put request merchantId not exists to database`() {
        val generateMerchantConfiguration = generateMerchantConfiguration()
        mockMvc.perform(
                put("/merchant-configuration")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateMerchantConfiguration))
        )
                .andExpect(status().isNotFound)
                .andExpect(content().string("MerchantId is not exists"))
    }

}
