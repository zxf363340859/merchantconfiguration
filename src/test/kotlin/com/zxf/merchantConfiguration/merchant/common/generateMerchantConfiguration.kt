package com.zxf.merchantConfiguration.merchant.common

import com.zxf.merchantConfiguration.merchant.enum.MerchantStatus
import com.zxf.merchantConfiguration.merchant.request.AddressRequest
import com.zxf.merchantConfiguration.merchant.request.MerchantConfigurationRequest
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import kotlin.random.Random


internal fun generateMerchantConfiguration(): MerchantConfigurationRequest {
    val random = Random
    val randomMerchantId = random.nextInt(10)
    val result = MerchantConfigurationRequest()
    result.merchantId = 1
    result.name = "test name $randomMerchantId"
    val sdf = SimpleDateFormat("yyyy-MM-dd")
    result.invoiceDate = sdf.parse(LocalDate.now().toString())
    result.ABN = "ABC"
    result.status = MerchantStatus.Active
    val address = AddressRequest()
    address.country = "demo country $randomMerchantId"
    address.state = "demo state $randomMerchantId"
    address.suburb = "demo suburb $randomMerchantId"
    address.street = "demo street $randomMerchantId"
    address.description = "demo description $randomMerchantId"
    result.address = address
    result.description = "demo merchant description $randomMerchantId"
    return result
}
