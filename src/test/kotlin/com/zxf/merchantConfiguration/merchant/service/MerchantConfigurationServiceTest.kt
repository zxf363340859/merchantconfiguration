package com.zxf.merchantConfiguration.merchant.service

import com.zxf.merchantConfiguration.merchant.exception.MerchantNotExistsException
import com.zxf.merchantConfiguration.merchant.common.generateMerchantConfiguration
import com.zxf.merchantConfiguration.merchant.repository.MerchantConfigurationRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class MerchantConfigurationServiceTest {
    @Autowired
    lateinit var repository: MerchantConfigurationRepository
    @Autowired
    lateinit var service: MerchantConfigurationService

    @Test
    internal fun `should be update merchant configuration  when give merchant id exit database`() {
        val merchantConfiguration = generateMerchantConfiguration()
        repository.saveAndFlush(merchantConfiguration.toEntity())

        val dbMerchantConfiguration = repository.findById(merchantConfiguration.merchantId).get()
        dbMerchantConfiguration.name = "updateName"

        service.save(dbMerchantConfiguration)

        Assertions.assertEquals(1, repository.findAll().size)
        Assertions.assertTrue(repository.findById(dbMerchantConfiguration.merchantId).get() == dbMerchantConfiguration)
    }

    @Test
    internal fun `should be insert merchant configuration  when give merchant id no exit database`() {
        Assertions.assertEquals(0, repository.findAll().size)
        val merchantConfiguration = generateMerchantConfiguration().toEntity()

        val merchantId = service.save(merchantConfiguration)

        Assertions.assertEquals(1, repository.findAll().size)
        Assertions.assertEquals(merchantConfiguration, repository.findById(merchantId).get())
    }

    @Test
    internal fun `should be throw not found exception when give merchantId not exists`() {
        Assertions.assertEquals(0, repository.findAll().size)
        val merchantConfiguration = generateMerchantConfiguration().toEntity()

        Assertions.assertThrows(MerchantNotExistsException::class.java) {
            service.update(merchantConfiguration)
        }
    }
}
