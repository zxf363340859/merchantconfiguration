package com.zxf.merchantConfiguration

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MerchantConfigurationApplication

fun main(args: Array<String>) {
	runApplication<MerchantConfigurationApplication>(*args)
}
