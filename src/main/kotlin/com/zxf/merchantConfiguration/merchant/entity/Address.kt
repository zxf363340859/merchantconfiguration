package com.zxf.merchantConfiguration.merchant.entity

import javax.persistence.*

@Table(name = "address")
@Entity
class Address(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = 0,
        @Column(nullable = false, length = 20)
        var country: String,
        @Column(nullable = false, length = 20)
        var state: String,
        @Column(nullable = false, length = 20)
        var suburb: String,
        @Column(nullable = true, length = 50)
        var street: String,
        @Column(nullable = true)
        var description: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Address) return false

        if (id != other.id) return false
        if (country != other.country) return false
        if (state != other.state) return false
        if (suburb != other.suburb) return false
        if (street != other.street) return false
        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + country.hashCode()
        result = 31 * result + state.hashCode()
        result = 31 * result + suburb.hashCode()
        result = 31 * result + street.hashCode()
        result = 31 * result + description.hashCode()
        return result
    }
}
