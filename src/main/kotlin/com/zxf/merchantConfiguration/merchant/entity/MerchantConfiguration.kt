package com.zxf.merchantConfiguration.merchant.entity

import java.util.*
import javax.persistence.*

@Table(name = "merchant_configuration")
@Entity
class MerchantConfiguration(
        @Id
        var merchantId: Int = 0,
        @Column(nullable = false, length = 50)
        var name: String,
        @Column(nullable = false)
        @Temporal(TemporalType.DATE)
        var invoiceDate: Date,
        @Column(nullable = false, length = 50)
        var ABN: String,

        var status: String,

        @OneToOne(cascade = [CascadeType.ALL])
        var address: Address,

        @Column(nullable = true)
        var description: String
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MerchantConfiguration) return false

        if (merchantId != other.merchantId) return false
        if (name != other.name) return false
        if (invoiceDate != other.invoiceDate) return false
        if (ABN != other.ABN) return false
        if (status != other.status) return false
        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        var result = merchantId
        result = 31 * result + name.hashCode()
        result = 31 * result + invoiceDate.hashCode()
        result = 31 * result + ABN.hashCode()
        result = 31 * result + status.hashCode()
        result = 31 * result + description.hashCode()
        return result
    }
}
