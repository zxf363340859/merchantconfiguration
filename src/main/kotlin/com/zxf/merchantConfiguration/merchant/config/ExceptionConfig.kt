package com.zxf.merchantConfiguration.merchant.config

import com.zxf.merchantConfiguration.merchant.exception.MerchantNotExistsException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionConfig {
    @ExceptionHandler(MerchantNotExistsException::class)
    fun handler(exception: MerchantNotExistsException):ResponseEntity<String> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.message)
    }
}
