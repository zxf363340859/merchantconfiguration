package com.zxf.merchantConfiguration.merchant.config

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.micrometer.core.instrument.MeterRegistry
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

@Aspect
@Component
class ControllerLog(final val registry: MeterRegistry) {
    val objectMapper = jacksonObjectMapper()
    val counterGet = registry.counter("counter_get")
    val counterPost = registry.counter("counter_post")

    val logger: Logger = LoggerFactory.getLogger(ControllerLog::class.java)

    @Pointcut("execution(* com.zxf.merchantConfiguration.merchant.controller..*.*(..))")
    fun webLog() {

    }

    @Before("webLog()")
    @Throws(Throwable::class)
    fun doBefore(joinPoint: JoinPoint) {
        val attributes = RequestContextHolder.getRequestAttributes() as ServletRequestAttributes?
        val request = attributes!!.request
        when (request.method) {
            "GET" -> counterGet.increment()
            "POST" -> counterPost.increment()
        }
        logger.info("========================================== request Start ==========================================")
        logger.info("URI            : {}", request.requestURI.toString())
        logger.info("HTTP Method    : {}", request.method)
        logger.info("Class Method   : {}.{}", joinPoint.signature.declaringTypeName, joinPoint.signature.name)
        logger.info("Request Args   : {}", objectMapper.writeValueAsString(joinPoint.args))
    }

    @After("webLog()")
    @Throws(Throwable::class)
    fun doAfter() {
        logger.info("=========================================== request End ===========================================")
    }


    @Around("webLog()")
    @Throws(Throwable::class)
    fun doAround(proceedingJoinPoint: ProceedingJoinPoint): Any {
        val startTime = System.currentTimeMillis()
        val result = proceedingJoinPoint.proceed()
        logger.info("Response Args  : {}", objectMapper.writeValueAsString(result))
        logger.info("Consuming time : {} ms", System.currentTimeMillis() - startTime)
        return result
    }
}
