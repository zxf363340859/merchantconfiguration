package com.zxf.merchantConfiguration.merchant.controller

import com.zxf.merchantConfiguration.merchant.request.MerchantConfigurationRequest
import com.zxf.merchantConfiguration.merchant.response.MerchantConfigurationResponse
import com.zxf.merchantConfiguration.merchant.service.MerchantConfigurationService
import io.micrometer.core.annotation.Timed
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("merchant-configuration")
@Timed
class MerchantConfigurationController(val merchantConfigurationService: MerchantConfigurationService) {

    @PostMapping()
    fun create(@RequestBody @Valid request: MerchantConfigurationRequest): ResponseEntity<Unit> {
        val merchantId = merchantConfigurationService.save(request.toEntity())
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("location", "/merchant/configuration/$merchantId")
                .build()
    }

    @PutMapping()
    fun update(@RequestBody @Valid request: MerchantConfigurationRequest): ResponseEntity<Unit> {
        merchantConfigurationService.update(request.toEntity())
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build()
    }

    @GetMapping()
    fun getMerchant(
            @RequestParam(name = "invoiceDate", required = false) invoiceDate: Long?,
            @RequestParam(name = "status", required = false) status: String?)
            : ResponseEntity<List<MerchantConfigurationResponse>> {
        val result = merchantConfigurationService.findByInvoiceDateOrStatus(invoiceDate, status)

        return ResponseEntity.ok()
                .body(result.map {
                    MerchantConfigurationResponse(
                            merchantId = it.merchantId,
                            name = it.name,
                            description = it.description,
                            invoiceDate = it.invoiceDate,
                            status = it.status
                    )
                })
    }

}
