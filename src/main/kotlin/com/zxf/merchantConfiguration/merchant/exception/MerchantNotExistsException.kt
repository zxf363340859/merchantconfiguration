package com.zxf.merchantConfiguration.merchant.exception

class MerchantNotExistsException(message: String) : RuntimeException(message) {
}
