package com.zxf.merchantConfiguration.merchant.repository

import com.zxf.merchantConfiguration.merchant.entity.MerchantConfiguration
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface MerchantConfigurationRepository : JpaRepository<MerchantConfiguration, Int> {
    @Query("from MerchantConfiguration where " +
            "(:status is null or status = :status)" +
            "and " +
            "(:date is null or invoiceDate = :date)")
    fun findByStatusOrInvoiceDate(status: String?, date: Date?): List<MerchantConfiguration>
}
