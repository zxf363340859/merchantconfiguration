package com.zxf.merchantConfiguration.merchant.service

import com.zxf.merchantConfiguration.merchant.entity.MerchantConfiguration
import com.zxf.merchantConfiguration.merchant.exception.MerchantNotExistsException
import com.zxf.merchantConfiguration.merchant.repository.MerchantConfigurationRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.*

@Service
class MerchantConfigurationService(
        val merchantConfigurationRepository: MerchantConfigurationRepository) {

    private val logger = LoggerFactory.getLogger(MerchantConfigurationService::class.java)

    fun save(merchantConfiguration: MerchantConfiguration): Int {
        return merchantConfigurationRepository.saveAndFlush(merchantConfiguration).merchantId
    }

    fun findByInvoiceDateOrStatus(invoiceDate: Long?, status: String?): List<MerchantConfiguration> {
        if (invoiceDate == null) {
            return merchantConfigurationRepository.findByStatusOrInvoiceDate(status, null)
        }
        val result = merchantConfigurationRepository.findByStatusOrInvoiceDate(status, Date(invoiceDate));
        return if (status == null) result.filter { it.status != "Disable" } else result
    }

    fun update(merchantConfiguration: MerchantConfiguration) {
        if (merchantConfigurationRepository.findById(merchantConfiguration.merchantId).isEmpty) {
            logger.warn("MerchantId is not exists merchantId:{}",merchantConfiguration.merchantId)
            throw MerchantNotExistsException("MerchantId is not exists")
        }
        merchantConfigurationRepository.saveAndFlush(merchantConfiguration).merchantId
    }
}
