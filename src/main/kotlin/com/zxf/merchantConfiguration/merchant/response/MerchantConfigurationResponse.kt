package com.zxf.merchantConfiguration.merchant.response

import java.util.*

class MerchantConfigurationResponse(
        val merchantId: Int,
        val name: String,
        val description: String,
        val invoiceDate: Date,
        val status: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MerchantConfigurationResponse

        if (merchantId != other.merchantId) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (invoiceDate != other.invoiceDate) return false
        if (status != other.status) return false

        return true
    }

    override fun hashCode(): Int {
        var result = merchantId
        result = 31 * result + name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + invoiceDate.hashCode()
        result = 31 * result + status.hashCode()
        return result
    }
}
