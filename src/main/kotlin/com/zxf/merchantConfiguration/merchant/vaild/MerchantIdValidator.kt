package com.zxf.merchantConfiguration.merchant.vaild

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class MerchantIdValidator : ConstraintValidator<MerchantIdConstraint, Int> {

    override fun isValid(value: Int, context: ConstraintValidatorContext?): Boolean {
        return value > 0
    }
}
