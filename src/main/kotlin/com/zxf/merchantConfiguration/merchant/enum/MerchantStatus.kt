package com.zxf.merchantConfiguration.merchant.enum

enum class MerchantStatus {
    Active, Suspended, Draft, Disable
}
