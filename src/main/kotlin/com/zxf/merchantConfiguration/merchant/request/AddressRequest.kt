package com.zxf.merchantConfiguration.merchant.request

import com.zxf.merchantConfiguration.merchant.entity.Address


class AddressRequest {
    var country: String = ""
    var state: String = ""
    var suburb: String = ""
    var street: String = ""
    var description: String = ""

    fun toEntity(): Address {
        return Address(
                country = this.country,
                state = this.state,
                suburb = this.suburb,
                street = this.street,
                description = this.description
        )
    }

}
