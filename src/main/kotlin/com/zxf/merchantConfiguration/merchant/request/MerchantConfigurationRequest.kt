package com.zxf.merchantConfiguration.merchant.request

import com.fasterxml.jackson.annotation.JsonFormat
import com.zxf.merchantConfiguration.merchant.entity.MerchantConfiguration
import com.zxf.merchantConfiguration.merchant.enum.MerchantStatus
import com.zxf.merchantConfiguration.merchant.vaild.MerchantIdConstraint
import java.util.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

class MerchantConfigurationRequest {
    @MerchantIdConstraint(message = "MerchantId needs to be greater than 0")
    var merchantId: Int = 0

    @NotEmpty
    var name: String = ""

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    lateinit var invoiceDate: Date

    var ABN: String = ""

    lateinit var status: MerchantStatus

    lateinit var address: AddressRequest

    var description: String = ""

    fun toEntity(): MerchantConfiguration {
        return MerchantConfiguration(
                merchantId = this.merchantId,
                name = this.name,
                invoiceDate = this.invoiceDate,
                ABN = this.ABN,
                status = this.status.name,
                address = this.address.toEntity(),
                description = this.description
        )
    }
}
